#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import math
import copy
import random
import pygame

from OpenGL.GL import *
from OpenGL.GLU import *
from dataclasses import dataclass

from line_full import Point, Vector, Line
from bsptree import BSPNode

def circle(center, radius, alpha):
    x = center.x + radius * math.cos(alpha)
    y = center.y + radius * math.sin(alpha)
    return Vector(x, y)


@dataclass
class Player:
    eye: Vector = Vector(5, 5)  # initial position
    alpha: float = math.radians(220)  # view direction


class FPSEngine2:

    def __init__(self, width=640, height=480, fov_x=math.radians(70), far=20):
        assert width > 0 and height > 0,
        self.__height = height
        self.__far = far

        self.__focal_length = width / (2 * math.tan(fov_x / 2))
        # the (triangular) region of space in the camera view that may appear on the screen
        # note that normals of both lines points inward
        self.__viewing_frustum = (Line(Point(-far * math.tan(fov_x / 2), far), Point(0, 0), None),
                                  Line(Point(0, 0), Point(far * math.tan(fov_x / 2), far), None))

        self.__player = Player()

        pygame.init()
        screen = pygame.display.set_mode(
            (width, height), pygame.OPENGL | pygame.DOUBLEBUF)

        glClipControl(GL_LOWER_LEFT, GL_NEGATIVE_ONE_TO_ONE)
        # make sure that the depth test is disabled, we won't be using z-buffer
        glDisable(GL_DEPTH_TEST)
        glDisable(GL_LIGHTING)
        glDisable(GL_CULL_FACE)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, self.__width, 0, self.__height, -1, 1)

    def InitScene(self, scene, wall_height=1):
        self.__wall_height = wall_height

        self.__scene = copy.deepcopy(scene)

        random.shuffle(self.__scene)
        # TODO put the root of the BSP tree here
        self.__scene_root = BSPNode(self.__scene[0])
        for line in self.__scene[1:]:
            self.__scene_root.insert(line)

    def LoadTextures(self, file_names):
        self.__textures = {}
        for file_name in file_names:
            texture = pygame.image.load(file_name)
            width = texture.get_width()
            height = texture.get_height()
            data = pygame.image.tostring(texture, "RGB", True)
            name = os.path.splitext(os.path.basename(file_name))[0]

            id = glGenTextures(1)
            glBindTexture(GL_TEXTURE_2D, id)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
                         0, GL_RGB, GL_UNSIGNED_BYTE, data)
            glGenerateMipmap(GL_TEXTURE_2D)
            glBindTexture(GL_TEXTURE_2D, 0)

            self.__textures[name] = id

        glActiveTexture(GL_TEXTURE0)
        glEnable(GL_TEXTURE_2D)

    def DrawWall(self, x0, x1, h0, h1, i0, i1, texture_name, u0, u1, show_borders=True):
        glBindTexture(GL_TEXTURE_2D, self.__textures[texture_name])

        t0 = u0 * h0
        t1 = u1 * h1

        glBegin(GL_TRIANGLE_STRIP)
        # A
        glNormal(0, 0, 1)
        glColor3f(i0, i0, i0)
        glTexCoord4f(t0, 0, 0, h0)
        glVertex3f(x0, (self.__height - h0) / 2, 0)
        # B
        glTexCoord4f(t0, h0, 0, h0)
        glVertex3f(x0, (self.__height + h0) / 2, 0)
        # C
        glColor3f(i1, i1, i1)
        glTexCoord4f(t1, 0, 0, h1)
        glVertex3f(x1, (self.__height - h1) / 2, 0)
        # D
        glTexCoord4f(t1, h1, 0, h1)
        glVertex3f(x1, (self.__height + h1) / 2, 0)
        glEnd()

        glBindTexture(GL_TEXTURE_2D, 0)

        if show_borders:
            glBegin(GL_LINE_LOOP)
            # A
            glColor3f(0.1, 0.95, 0.1)
            glVertex3f(max(x0, 1), (self.__height - h0) / 2, 0)
            # B
            glVertex3f(max(x0, 1), (self.__height + h0) / 2, 0)
            # C
            glVertex3f(max(x1, 1), (self.__height + h1) / 2, 0)
            # D
            glVertex3f(max(x1, 1), (self.__height - h1) / 2, 0)
            glEnd()

    def DrawLine(self, a, b, color=(1, 1, 1)):
        glBegin(GL_LINES)
        glColor3f(color[0], color[1], color[2])
        glVertex3f(a.x, a.y, 0)
        glVertex3f(b.x, b.y, 0)
        glEnd()

    def MainLoop(self):
        quit = False

        while not quit:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit = True
            keys = pygame.key.get_pressed()
            if keys[pygame.K_w]:
                self.__player.eye = circle(
                    self.__player.eye, 0.1, math.pi / 2 - self.__player.alpha)
            if keys[pygame.K_s]:
                self.__player.eye = circle(
                    self.__player.eye, -0.1, math.pi / 2 - self.__player.alpha)
            if keys[pygame.K_a]:
                self.__player.eye = circle(
                    self.__player.eye, 0.1, math.pi - self.__player.alpha)
            if keys[pygame.K_d]:
                self.__player.eye = circle(
                    self.__player.eye, -0.1, math.pi - self.__player.alpha)
            if keys[pygame.K_q]:
                self.__player.alpha -= math.radians(1)
            if keys[pygame.K_e]:
                self.__player.alpha += math.radians(1)

            glClearColor(0, 0, 0, 0)
            glClear(GL_COLOR_BUFFER_BIT)

            lines = []

            for line in self.__viewing_frustum:
                lines.append((line, (1, 1, 0)))

            viewer = Point(self.__player.eye.x, self.__player.eye.y)

            # TODO replace this line with BSP tree in-order traverse
            for line in self.__scene_root.draw(viewer):
                # camer space transform
                line_cs = line.affine_transform(-self.__player.eye,
                                                self.__player.alpha)
                lines.append((line_cs, (0.5, 0.5, 0.5)))

                # clipping
                front_line, _ = line_cs.split_by(self.__viewing_frustum[0])
                if front_line is not None:
                    visible_line, _ = front_line.split_by(
                        self.__viewing_frustum[1])
                    if visible_line is None:
                        continue
                else:
                    continue
                lines.append((visible_line, (0.5, 1, 0.5)))

                # transform visible line from camera space back to world space
                # texture coordinates are derived from world space coordinates
                visible_line_ws = visible_line.affine_transform(
                    Vector(), -self.__player.alpha, 1, self.__player.eye)
                # compute texture coordinates at line ends
                alpha = math.atan2(visible_line_ws.b.y - visible_line_ws.a.y,
                                   visible_line_ws.b.x - visible_line_ws.a.x)
                cx = math.cos(alpha)
                cy = math.sin(alpha)
                u0 = visible_line_ws.a.x * cx + visible_line_ws.a.y * cy
                u1 = visible_line_ws.b.x * cx + visible_line_ws.b.y * cy

                # just alias
                pa = visible_line.a
                pb = visible_line.b

                # scaling
                pax = pa.x * self.__focal_length / pa.y + self.__width / 2
                ha = self.__wall_height * self.__focal_length / pa.y
                pbx = pb.x * self.__focal_length / pb.y + self.__width / 2
                hb = self.__wall_height * self.__focal_length / pb.y

                # compute attenuation factors for both line ends
                sa = max(
                    0, min(1, (1 - Vector(pa.x, pa.y).length() / self.__far)**2))
                sb = max(
                    0, min(1, (1 - Vector(pb.x, pb.y).length() / self.__far)**2))

                self.DrawWall(pax, pbx, ha, hb, sa, sb, line.material, u0, u1)

            for line, color in lines:
                line_ss = line.affine_transform(
                    Vector(0, 0), 0, 10, Vector(self.__width / 2, 100))
                self.DrawLine(line_ss.a, line_ss.b, color)

            pygame.display.flip()
            pygame.time.wait(30)


def main():
    # create scene
    scene = [
        Line(Point(0, 0), Point(5, 0), "Wall_C_02"),
        Line(Point(5, 0), Point(6, 0), "Wide_Door_01"),
        Line(Point(6, 0), Point(10, 0), "Wall_C_02"),
        Line(Point(10, 0), Point(10, 10), "Wall_C_02"),
        Line(Point(10, 10), Point(5.5, 10), "Wall_C_02"),
        Line(Point(4.5, 10), Point(0, 10), "Wall_C_02"),
        Line(Point(0, 10), Point(0, 0), "Wall_C_02"),

        Line(Point(2, 2), Point(4, 2), "Wall_A_02"),
        Line(Point(4, 2), Point(4, 4), "Wall_A_02"),
        Line(Point(4, 4), Point(2, 4), "Wall_A_02"),
        Line(Point(2, 4), Point(2, 2), "Wall_A_02"),

        Line(Point(8, 8), Point(8, 6), "Wall_A_02"),
        Line(Point(8, 6), Point(6, 8), "Wall_A_02"),
        Line(Point(6, 8), Point(8, 8), "Wall_A_02"),

        Line(Point(6, 2), Point(7, 2), "Wall_A_02"),
        Line(Point(7, 2), Point(7, 3), "Wall_A_02"),
        Line(Point(7, 3), Point(8, 3), "Wall_A_02"),
        Line(Point(8, 3), Point(8, 4), "Wall_A_02"),
        Line(Point(6, 2), Point(8, 4), "Wall_A_02"),

        Line(Point(4.5, 10), Point(4.5, 11), "Wall_B_02"),
        Line(Point(5.5, 10), Point(5.5, 11), "Wall_B_02"),

        Line(Point(3, 11), Point(4.5, 11), "Wall_A_02"),
        Line(Point(5.5, 11), Point(7, 11), "Wall_A_02"),
        Line(Point(7, 11), Point(7, 15), "Wall_A_02"),
        Line(Point(7, 15), Point(6, 15), "Wall_A_02"),
        Line(Point(6, 15), Point(5, 15), "Wide_Door_02"),
        Line(Point(5, 15), Point(3, 15), "Wall_A_02"),
        Line(Point(3, 15), Point(3, 11), "Wall_A_02"),

        Line(Point(3.707, 7.707), Point(4.000, 7.000), "Wall_A_02"),
        Line(Point(3.000, 8.000), Point(3.707, 7.707), "Wall_A_02"),
        Line(Point(2.293, 7.707), Point(3.000, 8.000), "Wall_A_02"),
        Line(Point(2.000, 7.000), Point(2.293, 7.707), "Wall_A_02"),
        Line(Point(2.293, 6.293), Point(2.000, 7.000), "Wall_A_02"),
        Line(Point(3.000, 6.000), Point(2.293, 6.293), "Wall_A_02"),
        Line(Point(3.707, 6.293), Point(3.000, 6.000), "Wall_A_02"),
        Line(Point(3.707, 6.293), Point(4.000, 7.000), "Wall_A_02"),
    ]

    engine = FPSEngine2(1280, 720)
    engine.LoadTextures(["Wall_A_02.png", "Wall_B_02.png",
                        "Wall_C_02.png", "Wide_Door_01.png", "Wide_Door_02.png"])
    engine.InitScene(scene)
    engine.MainLoop()


if __name__ == "__main__":
    main()
