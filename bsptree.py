#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from errno import ESTALE
from lib2to3.pgen2.tokenize import generate_tokens
from locale import setlocale
from sys import api_version
import line_full

__version__ = "0.1.0"

class BSPNode(object):
    """A class representing a binary space partitioning tree node."""

    def __init__(self, value):
        """Initialize node members."""
        self.__value = value  # splitting line
        self.__left = None
        self.__right = None

    def depth(self):
        """Return the depth (height) of a tree counting from this node."""
        d = -1
        if self.__left:
            d = self.__left.depth()
        if self.__right:
            d = max(d, self.__right.depth())
        return d + 1

    def insert(self, value):

        (front, back) = value.split_by(self.__value)
        if front is not None:
            if self.__left == None:
                self.__left = BSPNode(front)
            else:
                self.__left.insert(front)

        if back is not None:
            if self.__right == None:
                self.__right = BSPNode(back)
            else:
                self.__right.insert(back)

    def traverse_inorder(self, viewer):
        normal = self.__value.normal()
        vektor = viewer - self.__value.b
        vysledek = vektor.dot(normal)

        if vysledek > 0:
            if self.__right is not None:
                yield from self.__right.traverse_inorder(viewer)
            yield self.__value
            if self.__left is not None:
                yield from self.__left.traverse_inorder(viewer)
        else:
            if self.__left is not None:
                yield from self.__left.traverse_inorder(viewer)
            yield self.__value
            if self.__right is not None:
                yield from self.__right.traverse_inorder(viewer)
        """
    In-order depth first traversal gives nodes in non-decreasing order in case of BST and back-to-front in case of BSP tree.
    When calling recursive function, you need to use 'yield from' proposed in PEP 380, see https://www.python.org/dev/peps/pep-0380/.
    """

    def draw(self, viewer):
        sorted_values = []
        traversal = self.traverse_inorder(viewer)
        while True:
            try:
                sorted_values.append(next(traversal))
            except StopIteration:
                break
        return sorted_values


def main():
    pass


if __name__ == "__main__":
    main()
