#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math

class Point():

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y)

    def __add__(self, other):
        if type(other) is Vector:
            return Point(self.x + other.x, self.y + other.y)
        raise TypeException

    def __str__(self):
        return f"[{self.x:.3f}, {self.y:.3f}]"


class Vector(Point):

    def __init__(self, x=0.0, y=0.0):
        super().__init__(x, y)

    def length(self):
        return math.sqrt(self.x**2 + self.y**2)

    def normalize(self):
        length = self.length()
        if length > 0:
            tmp = 1 / length
            self.x *= tmp
            self.y *= tmp

    def __sub__(self, other):
        if isinstance(other, (Vector,)):
            return super().__sub__(other)
        raise TypeException

    def __neg__(self):
        return Vector(-self.x, -self.y)

    def __add__(self, other):
        if isinstance(other, Vector):
            return Vector(self.x + other.x, self.y + other.y)
        raise TypeException

    def __mul__(self, scalar):
        return Vector(self.x * scalar, self.y * scalar)

    def dot(self, other):
        return self.x * other.x + self.y * other.y

    def __str__(self):
        return f"({self.x:.3f}, {self.y:.3f})"


class Line(object):

    def __init__(self, point_a, point_b, material=None):
        self.a = point_a
        self.b = point_b
        self.material = material

    def direction(self):
        return self.b - self.a

    def normal(self, normalize=False):
        normal = self.b - self.a
        normal.x, normal.y = -normal.y, normal.x
        if normalize:
            normal.normalize()
        return normal

    def mid_point(self):
        return Point((self.a.x + self.b.x) / 2, (self.a.y + self.b.y) / 2)

    def intersection(self, other, on_first=True, on_second=True):

        d1 = self.direction()
        d2 = other.direction()
        det = d1.y * d2.x - d1.x * d2.y
        if det != 0:
            da = other.a - self.a
            t1 = (d2.x * da.y - d2.y * da.x) / det
            t2 = (d1.x * da.y - d1.y * da.x) / det
            inside1 = t1 > 0 and t1 < 1
            inside2 = t2 > 0 and t2 < 1
            if (not on_first or (on_first and inside1)) and (not on_second or (on_second and inside2)):
                i1 = self.a + d1 * t1
                i2 = other.a + d2 * t2
                return i1
        return None

    def split_by(self, other):

        i = self.intersection(other, True, False)

        if i is None:
            front_line = self
            back_line = None
        else:
            front_line = Line(self.a, i, self.material)
            back_line = Line(i, self.b, self.material)

        # is "front line" really in front of the other one?
        if (front_line.mid_point() - other.a).dot(other.normal()) < 0:
            front_line, back_line = back_line, front_line

        return (front_line, back_line)

    def affine_transform(self, pre_translation, rotation, scale=1, post_translation=Vector(0, 0)):
        # pre translation
        a = self.a + pre_translation
        b = self.b + pre_translation
        # rotation
        cos = math.cos(rotation)
        sin = math.sin(rotation)
        a = Point(a.x * cos - a.y * sin, a.x * sin + a.y * cos)
        b = Point(b.x * cos - b.y * sin, b.x * sin + b.y * cos)
        # scale
        a.x *= scale
        a.y *= scale
        b.x *= scale
        b.y *= scale
        # post translation
        a += post_translation
        b += post_translation

        return Line(a, b, self.material)

    def __str__(self):
        return f"[{self.a} -> {self.b}]"


def main():
    a = Point(2, 3)
    print(a)

    b = Point(9, 4)
    d = b - a
    print(d)
    print(d * 3)
    print(d.length())
    d.normalize()
    print(d.length())

    line = Line(a, b)
    print(line)
    normal = line.normal()
    print(normal)
    print(normal.dot(d))

    line1 = Line(Point(1, 1), Point(4, 4))

    print(line1.normal())
    line2 = Line(Point(2, 4), Point(2, 5))
    print("line2 normal is " + str(line2.normal()))
    print(line1.intersection(line2))

    print("split " + str(line1) + " by " + str(line2))
    front_line, back_line = line1.split_by(line2)
    print(str(front_line) + " is in front")
    print(str(back_line) + " is in back")


if __name__ == "__main__":
    main()
